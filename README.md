## Clusterization pipeline using HBase
## Installation
Installed HBase, Haddop and Spark following this tutorial :

https://kontext.tech/thread/628/spark-connect-to-hbase

Didn't use docker because of hbase setup issues

## Load data to DB
Loaded data to HBase following this:

https://docs.cloudera.com/cdsw/1.9.2/import-data/topics/cdsw-load-data-into-hbase-table.html

## Get data from HBase

Used happybase to load out data from DB to pyspark. After this, converted to pyspark DataFrame

## Pipeline

Configured and Ran pipeline with clusterization and logistic regression

## Metrics

Computed all the metrics and plotted graph

![](report/written.png)
![](report/regression.png)