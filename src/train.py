import pandas as pd
import traceback
import matplotlib.pyplot as plt

from pyspark.ml.feature import VectorAssembler
from pyspark.ml.clustering import KMeans
from pyspark.sql.types import StructField, FloatType, StructType
from pyspark.ml.classification import LogisticRegression
from pyspark.ml import Pipeline
from pyspark.ml.feature import Normalizer

from utils import SparkAdapter
from unload_db import DBhandler

from logger import Logger
from utils import SparkAdapter

SHOW_LOG = True
N_CLUSTERS = 4
MAX_ITER = 25
NUM_PARTITIONS = 60
SEED = 42


class Trainer():

    def __init__(self):
        """
        Initialize logger
        """
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.log.info("Trainer Initialized")
        
    def load_data(self):
        """
        Load first one third of data rows for convenience to pandas DataFrame 
        """
        self.log.info(f'Loading data')
        db_hand = DBhandler('food_in')
        self.full_data = db_hand.dump_to_np()
        if self.full_data.shape[0] > 100000:
            self.full_data = self.full_data[:100000]
        self.pd_df = pd.DataFrame(self.full_data)
        self.log.info(f'Got data shaped {self.full_data.shape}')
        self.schema = StructType([StructField("1", FloatType(), True),
                            StructField("2", FloatType(), True),
                            StructField("3", FloatType(), True),
                            StructField("4", FloatType(), True),
                            StructField("5", FloatType(), True),
                            StructField("6", FloatType(), True),
                            StructField("7", FloatType(), True),
                            StructField("8", FloatType(), True),
                            StructField("9", FloatType(), True),
                            StructField("10", FloatType(), True),
                            StructField("11", FloatType(), True),
                            StructField("12", FloatType(), True),])

    def to_matrix(self, data):
        """
        Assemble Data to Feature Vector to process 
        """
        vecAssembler = VectorAssembler(inputCols=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"], outputCol="features")
        new_df = vecAssembler.transform(data)
        return new_df

    def unload_data(self, data, with_prediction=False):
        """
        Write Data to DataBase
        """
        self.log.info(f'Writing data')
        db_hand = DBhandler('food_out')
        self.full_data = db_hand.dump_to_db(data, with_prediction)

    def asess(self, model):
        """
        Get all the metrics of a model
        """
        clustering_summary = model.stages[0].summary
        self.log.info(f'Cluster sizes: {clustering_summary.clusterSizes}')
        self.log.info(f'Cluster cost (elbow): {clustering_summary.trainingCost}')
        classifing_summary = model.stages[1].summary
        self.log.info(f'Accuracy: {classifing_summary.accuracy}')
        self.log.info(f'Precision: {classifing_summary.weightedPrecision}')
        self.log.info(f'Recall: {classifing_summary.weightedRecall}')
        self.log.info(f'TPR: {classifing_summary.weightedTruePositiveRate}')
        self.log.info(f'FPR: {classifing_summary.weightedFalsePositiveRate}')
        self.log.info(f'Loss history: {classifing_summary.objectiveHistory}')
        self.draw_training(classifing_summary)

    def compute_prob(self, model, data):
        """
        Get class to cluster match probability for linear regression
        """
        from operator import add
        df_prediction = model.transform(data)
        df_normed = df_prediction.rdd.map(lambda x: (x['class'], x['probability'])) \
            .reduceByKey(add).toDF(['class', 'probability'])
        normer = Normalizer(inputCol='probability', outputCol='normed', p=1)
        normed = normer.transform(df_normed).rdd.map(lambda x: (x['class'], x['normed'].toArray().max()))
        for label, confidence in normed.collect():
            self.log.info(f'label: {int(label)} probability: {confidence:.4f}')

    def draw_training(self, classifing_summary):
        """
        Draw losses for regression
        """
        plt.title('Regression')
        plt.plot(classifing_summary.objectiveHistory)
        plt.savefig('report/regression.png')

    def train(self):
        """
        Looad data
        Convert to desired types
        Configure Pipeline
        Run it
        Get Metrics
        """
        try:
            adapter = SparkAdapter()
            context = adapter.get_context()
            session = adapter.get_session()
        except:
            self.log.error(traceback.format_exc())
            return False

        self.load_data()

        # create DF
        spark_df = session.createDataFrame(self.pd_df, schema=self.schema)
        
        # Mapped to Matrix
        self.log.info('Data To Matrix')
        new_df = self.to_matrix(spark_df)

        
        self.log.info('Configuring Pipeline')
        clustering = KMeans(
            k=N_CLUSTERS,
            featuresCol='features',
            predictionCol='cluster',
            seed=SEED
        )

        classifing = LogisticRegression(
            maxIter=MAX_ITER,
            family='multinomial',
            featuresCol='features',
            labelCol='cluster',
            predictionCol='class'
        )

        self.log.info('Pipeline starting')
        pipeline = Pipeline(stages=[clustering, classifing])
        model = pipeline.fit(new_df)

        self.asess(model=model)
        self.compute_prob(model=model, data=new_df)

if __name__ == "__main__":
    trainer = Trainer()
    trainer.train()