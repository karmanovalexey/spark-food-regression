from collections import OrderedDict
from natsort import natsorted
import numpy as np
import happybase


class DBhandler():
    """
    Before this, start an hbase server and a thrift one:
    ~/Study/hbase-2.4.15/bin/hbase-daemon.sh start thrift -p 9090 --infoport 9091
    ~/Study/hbase-2.4.15/bin/start-hbase.sh
    """
    def __init__(self, table_name='food_in'):
        """
        Open a connection to hbase table
        """
        c = happybase.Connection('127.0.0.1',9090)
        self.table = c.table(table_name)

    def row_to_vector(self, row):
        """
        Converts raw row to numpy array
        """
        d = {}
        for col, val in row.items():
            d[col.decode()[:-1]] = float(val.decode())
        row = OrderedDict(natsorted(d.items()))
        return [val for val in row.values()]

    def dump_to_np(self):
        """
        Reads table and outputs matrix
        """
        arr = []
        for key, data in self.table.scan():
            data = self.row_to_vector(data)
            arr.append(data)
        return np.array(arr).astype(np.float32)

    def vector_to_row(self, vector, with_prediction=False):
        """
        Converts numpy array to raw row
        """
        if with_prediction:
            base_vector = vector[:-1]
            row = {(str(key)+':').encode(): str(value).encode() for key, value in zip(np.arange(1, len(base_vector)+1), base_vector)}
            row['clusters:'.encode()] = str(vector[-1]).encode()
        else:
            row = {(str(key)+':').encode(): str(value).encode() for key, value in zip(np.arange(1, len(vector)+1), vector)}
        return row

    def dump_to_db(self, data, with_prediction=False):
        """
        Writes numpy matrix to table
        """
        for i, line in enumerate(data):
            row = self.vector_to_row(line, with_prediction)
            self.table.put(str(i).encode(), row)

if __name__ == '__main__':
    hand = DBhandler()
    full_data = hand.dump_to_np()